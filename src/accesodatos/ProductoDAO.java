
package accesodatos;

import Servicio.Conecta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import negocio.Producto;

/**
 *
 * @author Jazna
 */
public class ProductoDAO {
    private Producto p;

    public ProductoDAO() {
    }

    public ProductoDAO(Producto p) {
        this.p = p;
    }
    
    /**
     * Método que usted debe implementar
     */
    public static boolean grabar(String user, String pass, Producto p){
        
         
        try{
            Connection con = Conecta.getConecta(user, pass);
            String query = "insert into producto(pr_id, pr_nombre, pr_moneda, pr_valormoneda, pr_pesos) values(?,?,?,?, ?)";
            PreparedStatement insert = con.prepareStatement(query);
            insert.setInt(1, p.getId());
            insert.setString(2, p.getNombre());
            insert.setString(3, p.getMoneda());
            insert.setDouble(4, p.getValorMoneda());
            insert.setDouble(5, p.getValorPesos());
            insert.execute();
            insert.close();
            con.close();
            return true;
            
            
            
        }catch(SQLException e)
        {
            javax.swing.JOptionPane.showMessageDialog(null, " Error al realizar operación "+e.getMessage());
            return false;
        }
    }
    
    public static ResultSet buscar(String user, String pass, int cod)
    {
        ResultSet rs= null;
        try{
            Connection con = Conecta.getConecta(user, pass);
            String query = "select * from producto where pr_id=?";
            PreparedStatement buscar = con.prepareStatement(query);
            buscar.setInt(1, cod);
            rs = buscar.executeQuery();
            
        }catch(SQLException e)
        {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al realizar operación  "+e.getMessage());
        }
        return rs;
    }
    
    public static int validar(String user, String pass, int cod)
    {
        int b= 0;
        try{
            Connection con = Conecta.getConecta(user, pass);
            String query = "select * from producto where pr_id=?";
            PreparedStatement validar = con.prepareStatement(query);
            validar.setInt(1, cod);
            b = validar.executeUpdate();
            
        }catch(SQLException e)
        {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al realizar operación  "+e.getMessage());
            b=-1;
        }
        return b;
    }
}
