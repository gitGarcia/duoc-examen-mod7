
package servicio;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class ConectarMindicador {
    
    public static double traerValor(String s){
        double d=0;
        
        try{
            
        
        URL url = new URL("http://mindicador.cl/api/" + s);
        InputStream is = url.openStream();
        JsonReader reader = Json.createReader(is);
        JsonObject obj = reader.readObject();
        
        d = Double.parseDouble(obj.getJsonArray("serie").getJsonObject(0).get("valor").toString());
        }catch(Exception e)
        {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al consultar valor divisa" + e.getMessage());
        }
        
        return d;
        }

    
    
    
    
    
    
    
}
