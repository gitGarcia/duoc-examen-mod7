
package Servicio;
import java.awt.HeadlessException;
import java.sql.*;


public class Conecta {

    public static Connection getConecta(String user, String pass) {

        Connection con = null;

        try {

            Class.forName("oracle.jdbc.OracleDriver");
            String url = "jdbc:oracle:thin:@localhost:1521:XE";
            con = DriverManager.getConnection(url, user, pass);

        } catch (HeadlessException | ClassNotFoundException | SQLException e) {

            javax.swing.JOptionPane.showMessageDialog(null, "Conexion sin realizar " + e.getMessage());

        }
        return (con);
    }

    
}
