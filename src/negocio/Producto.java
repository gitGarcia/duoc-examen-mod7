
package negocio;


public class Producto {
    private int id;
    private String nombre, moneda;
    private double valorMoneda, valorPesos;

    public Producto() {
    }

    public Producto(int id) {
        this.id = id;
    }

    public Producto(int id, String nombre, String moneda, double valorMoneda, double valorPesos) {
        this.id = id;
        this.nombre = nombre;
        this.moneda = moneda;
        this.valorMoneda = valorMoneda;
        this.valorPesos = valorPesos;
    }



    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public double getValorMoneda() {
        return valorMoneda;
    }

    public void setValorMoneda(double valorMoneda) {
        this.valorMoneda = valorMoneda;
    }

    public double getValorPesos() {
        return valorPesos;
    }

    public void setValorPesos(double valorPesos) {
        this.valorPesos = valorPesos;
    }

    
    
    
    
}
